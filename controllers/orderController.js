const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order");
const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports.checkout = async (data) => {
	//console.log(data)
	let newOrder = await new Order ({
		userId : data.userId,
		productId : data.productId,
		totalAmount : data.amount
	})
	//console.log(newOrder)
	return newOrder.save().then((order, error) => {
		//console.log(order)
		if(error){
			return false;
		}else{
			let userSaveStatus = User.findById(data.userId).then(user => {
				//console.log(user.orders)
				user.orders.push({orderId: order._id})
				return user.save().then((user, err) => {
					if(err){
						return false;
					}else{
						return true;
					}
				})
			})
		}

	})

	// if(newOrder && userSaveStatus === true){
	// 	return true;
	// }else{
	// 	return false;
	// }
}


module.exports.getAllOrders = () => {
	return Order.find().then(result => {
		//console.log(result)
		return result
	})
}

/*module.exports.checkout = async (data) => {
	
	let courseSaveStatus = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId})
		return course.save().then((course, err) => {
			if(err){
				return false; //user not added to enrollees
			}else{
				return true; //user successfully added to enrollees
			}
		})
	})

	let userSaveStatus = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId})
		return user.save().then((user, err) => {
			if(err){
				return false; //course not saved in user's enrollments
			}else{
				return true; //course successfully saved in user's enrollments
			}
		})
	})

	if(courseSaveStatus && userSaveStatus){
		return true;
	}else{
		return false;
	}

}*/