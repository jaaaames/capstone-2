const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order");
const auth = require("../auth");
const bcrypt = require("bcrypt");

//add a product
module.exports.addProduct = (body) => {
	let newProduct = new Product({
		productName: body.name,
		description: body.description,
		price: body.price
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//edit a product
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		productName : body.name,
		description : body.description,
		price : body.price
	}
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//get all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

//get specific product
module.exports.getProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result
	})
}

//archive product
module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}