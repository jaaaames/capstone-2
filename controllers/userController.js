const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order");
const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports.checkEmail = (body) => {
	return User.find({email: body.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (body) => {
	let newUser = new User({
		fullName: body.fullName,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return false;
			}
		}
	})
}

module.exports.setUserAsAdmin = (params, body) => {
	let adminUser = {
		isAdmin: true
	}
	console.log(body)
	return User.findByIdAndUpdate(body._id, adminUser).then((user, error) => {

		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.setUserAsNonAdmin = (params, body) => {
	let adminUser = {
		isAdmin: false
	}
	console.log(body)
	return User.findByIdAndUpdate(body._id, adminUser).then((user, error) => {

		if(error){
			return false;
		}else{
			return true;
		}
	})
}


module.exports.getUsersOrders = (data) => {
	return User.findById(data.userId).then(result => {
		//console.log(data.orders)
		return result.orders
	})
}
