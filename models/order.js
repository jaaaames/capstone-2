const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	orderDate : {
		type : Date,
		default : new Date()
	},
	userId : {
			type : String,
			required : [true, "UserId is required"]
	},
	productId : {
			type: String,
			required : [true, "Product Id is required"]
	},
	totalAmount : {
		type: Number,
		default: 1
	}
})

module.exports = mongoose.model("Order", orderSchema)