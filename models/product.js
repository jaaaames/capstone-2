const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName : {
		type : String,
		required : [true, "Product name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	}
})

module.exports = mongoose.model("Product", productSchema);