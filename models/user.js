const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	fullName : {
		type : String,
		required : [true, "Full name is required"]
	},
	email : {
		type: String,
		required : [true, "Email address is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders: [
		{
				orderId : {
				type : String,
				required : false
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);


/*

bearer token
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwZTBjMDI1MDVjNDllZjNmOTgwNzU1MiIsImVtYWlsIjoiamFtZXNAbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2MjU1Nzc4MDd9.wMi-Ndzq4ZBftEkC2NvOQHCNjb88g6rY0L9BxjiOYAc

course 1 id
60e1f8f2b64e6e5f0addfe85

course 2
60e21c90473f6f689a0b1e1f


*/


