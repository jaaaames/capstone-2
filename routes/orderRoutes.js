const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");

//get all active products
router.get("/all", (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		orderController.getAllOrders().then(resultFromGet => res.send(resultFromGet))
	}else{
		res.send({auth: "Not an admin"})
	}
})

module.exports = router
