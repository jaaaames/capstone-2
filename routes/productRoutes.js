const express = require('express');
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");

module.exports = router

//add a product
router.post("/", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})

//modify a product
router.post("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
	}else{
		res.send({auth: "Not an admin"})
	}
})

//get all active products
router.get("/all", (req, res) => {
	productController.getAllActive().then(resultFromGet => res.send(resultFromGet))
})

//get single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})

//archive a product
router.delete("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){	
		productController.archiveProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an admin"})
	}
})

