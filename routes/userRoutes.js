const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");


//check for duplicate emails
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister)) 
})

//log in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//set user as admin
router.put("/setAdmin", auth.verify, (req, res) => {
	//console.log(res)
	userController.setUserAsAdmin(req.params, req.body).then(resultFromSetUserAsAdmin => res.send(resultFromSetUserAsAdmin))
})

//set user as non-admin
router.put("/setAsNonAdmin", auth.verify, (req, res) => {
	//console.log(res)
	userController.setUserAsNonAdmin(req.params, req.body).then(resultFromSetUserAsAdmin => res.send(resultFromSetUserAsAdmin))
})

//user checkout
router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		amount: req.body.price
	}

	orderController.checkout(data).then(resultFromCheckout => res.send(resultFromCheckout))
})

//get user's orders
router.get("/orders", (req, res) => {
	const data = {userId: auth.decode(req.headers.authorization).id}
	userController.getUsersOrders(data).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})





module.exports = router